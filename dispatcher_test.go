package dispatcher

import (
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ds-go/router"
)

func Test_NewDispatcher(t *testing.T) {

	dir := "/"
	options := router.NewDefaultOptions()
	names := []string{"name"}

	expected := &dispatcher{
		r:        router.NewRouter(dir, options),
		handles:  make(map[string]Handler),
		m:        make(map[string]func() Middleware),
		name:     names,
		path:     dir,
		handlers: make(map[int]http.HandlerFunc),
	}

	actual := NewDispatcher(dir, options, names)
	assert.Equal(t, expected, actual)
}

func TestUrlVars(t *testing.T) {
	d := CreateMockDispatcher()
	d.Add("GET", "/path/:(a-z)-foo/:(a-z)-bar", nil, nil)

	r := d.Match("GET", "/path/baz/bat")
	expected := make(map[string]interface{})
	expected["foo"] = "baz"
	expected["bar"] = "bat"

	assert.Equal(t, expected, r.GetVars())
}

func Test_SetMiddleware(t *testing.T) {
	d := CreateMockDispatcher()
	err := d.SetMiddleware("mock", MockMiddleware)
	assert.Nil(t, err)
}
func Test_SetMiddlewareDuplicate(t *testing.T) {
	d := CreateMockDispatcher()
	err := d.SetMiddleware("mock", MockMiddleware)
	err = d.SetMiddleware("mock", MockMiddleware)
	assert.NotNil(t, err)
}

func Test_GetMiddlewareNotFound(t *testing.T) {
	d := CreateMockDispatcher()
	_, err := d.GetMiddleware("name")
	assert.NotNil(t, err)
}

func Test_BuildQueryFromVars(t *testing.T) {

	expected := "foo=bar&baz=bat&pw=bud"
	v, _ := url.ParseQuery(expected)

	values := make(map[string]interface{})

	values["pw"] = v.Get("pw")
	values["baz"] = v.Get("baz")
	values["foo"] = v.Get("foo")

	actual := BuildQueryFromVars(values)
	actualvar := strings.Split(actual, "&")

	for _, actualval := range actualvar {
		av := strings.Split(actualval, "=")
		assert.Equal(t, values[av[0]], av[1])
	}
}

func Test_sliceContainsString(t *testing.T) {
	val := "someValue"
	slice := []string{
		val, "foo", "bar",
	}
	expected := true
	actual := _sliceContainsString(slice, val)
	assert.Equal(t, expected, actual)
}

func Test_sliceContainsStringEmpty(t *testing.T) {
	val := "someValue"
	slice := []string{
		"key", "foo", "bar",
	}
	expected := false
	actual := _sliceContainsString(slice, val)
	assert.Equal(t, expected, actual)
}

func CreateMockDispatcher() Dispatcher {
	return &dispatcher{
		r:       router.NewRouter("/", router.NewDefaultOptions()),
		handles: make(map[string]Handler),
		m:       make(map[string]func() Middleware),
		name:    []string{"name"},
		path:    "/",
	}
}

func MockMiddleware() Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			f(w, r)
		}
	}
}
