package dispatcher

import router "gitlab.com/ds-go/router"

//Response Dispatcher response contains response for route as well as additional hander information
type Response interface {
	GetRouterResponse() router.Response
	GetHandler() Handler
	GetStatusCode() int
	GetVars() map[string]interface{}
	GetNames() []string
}

type dispatchResponse struct {
	res     router.Response
	handler Handler
	code    int
}

//NewResponse Create a new dispatcher response
func NewResponse(response router.Response, h Handler, statusCode int) Response {
	return &dispatchResponse{
		res:     response,
		handler: h,
		code:    statusCode,
	}
}

func (r *dispatchResponse) GetNames() []string {
	return r.handler.GetNames()
}

func (r *dispatchResponse) GetVars() map[string]interface{} {
	return r.res.GetVars()
}

func (r *dispatchResponse) GetRouterResponse() router.Response {
	return r.res
}

func (r *dispatchResponse) GetHandler() Handler {
	return r.handler
}

func (r *dispatchResponse) GetStatusCode() int {
	return r.code
}
