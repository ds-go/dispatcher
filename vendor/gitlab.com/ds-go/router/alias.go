package router

// RouteAlias is a reference to it's parent route. They all share the same ID.
// Alias's are created for each possible match for that route.
// Optional param's will results in additional alias's
type RouteAlias interface {
	//GetID returns id for the alias/parent route
	GetID() string
	//GetURI returns uri string of current route.
	GetURI() string
	//GetIndex returns the number of params in this alias.
	GetArgCount() int
}

type alias struct {
	id  string
	uri string
	len int
}

func (r *alias) GetID() string {
	return r.id
}

func (r *alias) GetURI() string {
	return r.uri
}

func (r *alias) GetArgCount() int {
	return r.len
}

//NewRouteAlias creates a new route alias
//id string of route
//uri string of alias
//len int arg count
func NewRouteAlias(id string, uri string, len int) *alias {
	return &alias{id, uri, len}
}
