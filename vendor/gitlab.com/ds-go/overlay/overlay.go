package overlay

import (
	"fmt"
	"html/template"
	"io"
	"os"
	"path/filepath"
	"strings"
)

//Overlay collection of template overlays from master
type Overlay interface {
	Render(w io.Writer, layout string, data interface{}) error
	Extend(parent string, name string, file string) error
}

type theme struct {
	Master   *template.Template
	Overlays map[string]*template.Template
}

func (t *theme) Render(w io.Writer, layout string, data interface{}) error {
	overlay := t.Overlays[layout]
	err := overlay.Execute(w, data)
	if err != nil {
		return err
	}
	return nil
}

func (t *theme) Extend(parent string, name string, file string) error {

	tpl := t.Overlays[parent]
	if tpl == nil {
		tpl = t.Master
	}

	overlayTmpl, err := template.Must(tpl.Clone()).ParseFiles(file)
	if err != nil {
		return err
	}

	t.Overlays[name] = overlayTmpl
	return nil
}

func NewTheme(f string, dir string, o string) (Overlay, error) {

	master, err := template.New(f).ParseFiles(filepath.Join(dir, f))

	if err != nil {
		return nil, err
	}

	theme := &theme{
		Master:   master,
		Overlays: make(map[string]*template.Template),
	}

	err = filepath.Walk(filepath.Join(dir, o), func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}
		if info.IsDir() {
			return nil
		}

		name := strings.Replace(path, info.Name(), "", -1)
		name = strings.Replace(name, filepath.Join(dir, o), "", -1)
		name = strings.TrimLeft(name, "\\/\\\\")
		name = strings.Replace(name+info.Name(), `\`, `/`, -1)
		theme.Extend(f, name, path)

		return nil
	})

	if err != nil {
		return nil, err
	}

	return theme, nil
}
