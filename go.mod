module gitlab.com/ds-go/dispatcher

go 1.13

require (
	github.com/stretchr/testify v1.3.0
	gitlab.com/ds-go/overlay v0.0.0-20210920223109-0c86f5cea1aa
	gitlab.com/ds-go/router v0.0.0-20210920221739-56e12da00d56
)
